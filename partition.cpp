#include "stdafx.h"
#include "partition.h"

std::random_device rd;
std::mt19937 gen(rd());

inline std::vector<int>::iterator last_pivot(std::vector<int>::iterator p, std::vector<int>::iterator q)
{
  return q - 1;
}

inline std::vector<int>::iterator rand_pivot(std::vector<int>::iterator p, std::vector<int>::iterator q)
{
  std::uniform_int_distribution<int> dis(0,q - p - 1);
  return p + dis(gen);
}

inline std::vector<int>::iterator median_of_3_pivot(std::vector<int>::iterator a1, std::vector<int>::iterator a3)
{
  auto dist = a3 - a1 - 1;
  auto a2 = a1 + dist/2;
  // median of 3 
  if(*a1 >= *a2){
    if(*a2 >= *a3) return a2;
    else{
      if(*a1 >= *a3) return a3;
      else return a1;
    }
  }
  else{
    if (*a3 >= *a2) return a2;
    else{
      if(*a1 >= *a3) return a1;
      else return a3;
    }
  }
}


std::vector<int>::iterator partition(
                                     std::vector<int>::iterator p,
                                     std::vector<int>::iterator q)
{
  std::vector<int>::iterator pivot;

  pivot = last_pivot(p,q); // pivot selecting procedure
  std::iter_swap(pivot,q-1);

  pivot = q - 1;
  // p..j contains elements <= pivot
  auto j = p - 1;
  // walks over array with i for loop
  // when it encounters an element <= pivot
  // it moves it to the front of the array [p..q] 
  // but to the back of the array [p..j]
  // this is managed by j which gets swapped
  // with i every time an element <= pivot
  // is encountered and j is also incremented
  for(auto i = p; i < pivot; i++){ 
    if (*i <= *pivot){
      std::iter_swap(i,++j);
    }
  }
  // exchange pivot with last + 1 element of less than
  // pivot subarray p..j
  
  std::iter_swap(++j,pivot);
  
  return j;
}

std::vector<int>::iterator partition_rand(
                                     std::vector<int>::iterator p,
                                     std::vector<int>::iterator q)
{
  std::vector<int>::iterator pivot;
  // in general, we would swap the pivot
  // with the last element
  pivot = rand_pivot(p,q);
  
  auto tmp_ = *pivot;
  *pivot = *(q-1);
  *(q-1) = tmp_;

  pivot = q - 1;
  
  auto j = p - 1;
  for(auto i = p; i < q - 1; i++){
    if (*i <= *pivot){
      ++j;
      auto tmp = *j;
      *j = *i;
      *i = tmp;
    }
  }
  // exchange pivot with last element of less than
  // pivot subarray
  
  std::iter_swap(++j,pivot);

  return j;
}


std::vector<int>::iterator partition_median(
                                     std::vector<int>::iterator p,
                                     std::vector<int>::iterator q)
{
  std::vector<int>::iterator pivot;
  // in general, we would swap the pivot
  // with the last element
  pivot = median_of_3_pivot(p,q);
  
  auto tmp_ = *pivot;
  *pivot = *(q-1);
  *(q-1) = tmp_;

  pivot = q - 1;
  
  auto j = p - 1;
  for(auto i = p; i < q - 1; i++){
    if (*i <= *pivot){
      ++j;
      auto tmp = *j;
      *j = *i;
      *i = tmp;
    }
  }
  // exchange pivot with last element of less than
  // pivot subarray
  
  std::iter_swap(++j,pivot);

  return j;
}

