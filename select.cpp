#include "stdafx.h"
#include "partition.h"

int rand_select(
	std::vector<int>::iterator p, 
	std::vector<int>::iterator r,
	int i)
{
	if (p + 1 == r)
	{
		return *p;
	}
	// q points to 1 after the pivot
	auto q = partition_rand(p,r);
	// k is size of items, no -1
	auto k = q - p;
	// element at *q is fixed after partition
	// therefore the k-th order statistic is
	// in the right place. we check if the i-th
	// order statistic is below or above it
	// and do the appropriate recursive call
	if (i <= k)
	{
		return rand_select(p,q,i);
	}
	else
	{
		// no ++q since the pivot might have been the 
		// item to select, cant discard it
		return rand_select(q,r,i-k);
	}
}

int main()
{
	std::vector<int> a {1,4,2,3,5,7,10};
	std::cout << rand_select(a.begin(), a.end(),6) << "\n";
	return 0;
}
