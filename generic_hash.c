#include "stdio.h"

typedef unsigned char byte;

unsigned long 
generic_hash_sum (void* p, int size, unsigned long x)
{
  byte* ptr = (byte*) p;
  unsigned long sum = 0;
  for (int i = 0; i < size; ++i)
    {
      sum += *ptr++ * x;
    }
  return sum;
}

unsigned long 
generic_hash_horner (void* p, int size, unsigned long x)
{
  byte* ptr = (byte*) p;
  unsigned long sum = 0;
  for (int i = 0; i < size; ++i)
    {
      sum += x * (sum + *ptr++);
    }
  return sum;
}


typedef struct {
  int a;
  int b;
  char *c;
} my_struct;

int
main (int argc, char* argv[])
{
  unsigned long x = 31;
  /* int a = 2; */
  /* unsigned long h = generic_hash_horner (&a, sizeof(a), x); */
  /* printf ("%lu\n", h); */
  
  /* char* st = "trololol"; */
  /* h = generic_hash_horner (st, sizeof(st), x); */
  /* printf ("%lu\n", h); */

  /* char* st2 = "trolollo"; */
  /* h = generic_hash_horner (st2, sizeof(st2), x); */
  /* printf ("%lu\n", h); */

  /* my_struct m = {1,2,st}; */
  /* h = generic_hash_horner (&m, sizeof(m), x); */
  /* printf ("%lu\n", h); */

  /* h = generic_hash_horner (generic_hash_horner, sizeof(m), x); */
  /* printf ("%lu\n", h); */

  int a = 10;
  unsigned long h = generic_hash_horner (&a, sizeof(argv[1]), x);
  printf ("%lu\n", h);
  return 0;
}
