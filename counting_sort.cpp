#include "stdafx.h"

#include "counting_sort.h"

// 10 1 3 7
std::vector<int> CountingSort::sort(CountingSort::iterator_type begin, CountingSort::iterator_type end, int k)
{
  std::vector<int> C(k+1);
  std::vector<int> B(end - begin);

  for(auto i = begin; i < end; i++) C[*i]++;
  // C[i] now contains the counts for element i\n

  for(auto i = C.begin() + 1; i < C.end(); i++) *i += *(i-1);
  // C[i] now contains number of elements LEQ than i\n";

  for(auto i = end - 1; i >= begin; --i){
    B[C[*i] - 1] = *i; // -1 cause we index at 0
    C[*i]--; // if we have equal elements in the array, stable!
  }
  
  return B;
}


int main()
{
  int size;
  // mini tests
#if 1
  std::vector<int> a;
  
  a.push_back(10);
  a.push_back(1);
  a.push_back(3);
  a.push_back(7);

  std::vector<int> b = CountingSort::sort(a.begin(),a.end(),10);
  for(auto x: b) std::cout<<x<<" ";
  std::cout<<"\n";
#endif
  // benchmark
#if 1
  std::vector<int> c;
  size = 100000000;
  for(int i = 0; i < size; i ++){
    c.push_back(random() % (3*size/2));
  }
  std::vector<int> d = CountingSort::sort(c.begin(),c.end(),3*size/2 - 1);
  //std::sort(c.begin(),c.end());
  //std::cout<<"does counting sort work? "<<(c == d)<<"\n";
#endif
  return 0;
}
