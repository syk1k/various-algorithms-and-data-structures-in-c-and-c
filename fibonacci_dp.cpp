#include "stdafx.h"
// we assume the fibonacci sequence goes like
// 0 1 1 2 3 5... i.e. starts from 0 not 1
// therefore, the 1st item is 0, the 2nd is 1,
// the 3rd is 1, and the 4th is 2, etc.
unsigned long long int fibonacci_dp(long n)
{
  std::unordered_map<long, unsigned long long int > fib_map;
  fib_map[0] = 0;
  fib_map[1] = 1;

  for(auto i = 2; i < n ; i++){
    fib_map[i] = fib_map[i-1] + fib_map[i-2];
    fib_map.erase(i-2);
  }
  return fib_map[n-1];
}
int main()
{
  long i = 10000000L; // max
  long j = 3;
  long k = 4;
  long l = 5;
  long n = 6;
  long ar[]{j,k,l,n,i};
  for(auto a: ar)
    std::cout<<a<<"(rd|th) Fibonacci number is: "<<fibonacci_dp(a)<<"\n";
  return 0;
}
