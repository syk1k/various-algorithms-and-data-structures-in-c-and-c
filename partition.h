#include "stdafx.h"

std::vector<int>::iterator partition(
                                     std::vector<int>::iterator p,
                                     std::vector<int>::iterator q);
std::vector<int>::iterator partition_rand(
                                     std::vector<int>::iterator p,
                                     std::vector<int>::iterator q);
std::vector<int>::iterator partition_median(
                                     std::vector<int>::iterator p,
                                     std::vector<int>::iterator q);


