#include "stdafx.h"
#include "sorter.h"
#ifndef H_COUNTING_SORT_H
#define H_COUNTING_SORT_H

class CountingSort: protected Sorter<int>{
public:
  typedef Sorter<int>::iterator_type iterator_type;
  static std::vector<int> sort(Sorter<int>::iterator_type begin, Sorter<int>::iterator_type end, int k);
private:
  CountingSort(){};
  ~CountingSort(){};
  void operator= (CountingSort& other){};
  CountingSort (const CountingSort& other){};

};

#endif
