#include "stdafx.h"
#include <cstdlib>
bool is_palindrome(int x, int& y)
{
  if (x == 0){
    return true;
  }
  if (is_palindrome(x/10,y) && (x % 10 == y % 10)){
    y /= 10;
    std::cout<<"x is: "<<x<<" ; y is : "<<y<<"\n";
    return true;
  }
  else{
    return false;}
  return false;
}

bool is_palindrome_ll(unsigned int x){
  // need to figure out bit length of n
  int l = floor(log2(x)) + 1;
  unsigned int y = 0x1 << (l-1);
  unsigned int z = 0x1;
  for(int i =0; i < l; ++i){
    int a = x & y;
    int b = x & z;
    a = a >> (l - i -1);
    b = b >> i;
    if ((a ^ b) != 0x0) return false;
    y = y >> 1;
    z = z << 1;
  }
  return true;
}

bool is_palindrome(int x)
{
  return is_palindrome(x,x);
}


int main()
{
  int x = 1234;
  int y = 1234321;
  //std::cout<<"Is "<<x<<" a palindrome? "<<is_palindrome(x)<<"\n";
  //std::cout<<"Is "<<y<<" a palindrome? "<<is_palindrome(y)<<"\n";
  std::cout<<"LL: Is "<<8<<" a palindrome? "<<is_palindrome_ll(8)<<"\n";
  std::cout<<"LL: Is "<<9<<" a palindrome? "<<is_palindrome_ll(9)<<"\n";
  std::cout<<"LL: Is "<<21<<" a palindrome? "<<is_palindrome_ll(21)<<"\n";
  return 0;
}
