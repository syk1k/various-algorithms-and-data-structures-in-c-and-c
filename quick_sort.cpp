#include "stdafx.h"

#include "insertion_sort.h"
#include "heap_sort.h"
#include "partition.h"
// 19 4 1 7 2
void quick_sort(std::vector<int>::iterator begin, std::vector<int>::iterator end )
{
  if (begin + 1 < end){
    auto pivot = partition(begin,end);
    quick_sort(begin,pivot);
    quick_sort(++pivot,end);
  }
}

// hybrid quicksort
// from http://stackoverflow.com/questions/4056348/how-can-i-find-the-depth-of-a-recursive-function-in-c
struct stack_depth
{
  stack_depth() { ++counter; /*std::cout<<"incr counter "<<counter<<"\n";*/}
  ~stack_depth() { --counter; /*std::cout<<"decr counter "<<counter<<"\n";*/}
  static int counter;
};

int stack_depth::counter = 0;

void quick_sort_hybrid(
                std::vector<int>::iterator begin,
                std::vector<int>::iterator end,
                int size)
{
  stack_depth st;

  if(end - begin <= 64){  // .NET C++ uses 16, timsort uses 64
    insertion_sort(begin,end);
    return;
  }
  else{
    if(st.counter > size){
      MaxHeap::heap_sort(begin,end);
      return;
    }
    auto pivot = partition(begin,end);
    quick_sort_hybrid(begin,pivot,size);
    quick_sort_hybrid(++pivot,end,size);
  }
}
// hybrid quicksort with analysis of sortedness
// and diff pivot

double how_sorted(std::vector<int>::iterator begin, std::vector<int>::iterator end)
{
  int dist = end - begin - 1;
  int count = 0;
  for(auto i = begin; i < end; i++){ 
    if(*i <= *(i+1)) count++; // if array is sorted, branch prediction will guess very well 
  }
  return count*1.0/dist;
}

void quick_sort_hybrid_analysis_h(
                std::vector<int>::iterator begin,
                std::vector<int>::iterator end,
                int size)
{
  stack_depth st;

  if(end - begin <= 64){  // .NET C++ uses 16, timsort uses 64
    return insertion_sort(begin,end);
  }
  else{
    if(st.counter > size){
      return MaxHeap::heap_sort(begin,end);
    }
    std::vector<int>::iterator pivot = partition_median(begin,end);
    quick_sort_hybrid_analysis_h(begin,pivot,size);
    quick_sort_hybrid_analysis_h(++pivot,end,size);
  }
}

void quick_sort_hybrid_analysis(
                std::vector<int>::iterator begin,
                std::vector<int>::iterator end)
{
  int max_depth = (int) 2.0*log(end - begin)/log(2.0);
  if(how_sorted(begin,end) > 0.95) return insertion_sort(begin,end); // should be a function of the size and the
  else return quick_sort_hybrid_analysis_h(begin,end,max_depth);
}

// metric for measure of sortedness 

int main()
{
  int size;
#if 1
  // mini testing
  std::vector<int> v;
  v.push_back(19);
  v.push_back(4);
  v.push_back(1);
  v.push_back(7);
  v.push_back(2);
  
  quick_sort(v.begin(),v.end());
  for(auto x : v) std::cout<<x<<" ";
  std::cout<<"\n";

#endif
  
  //big testing
#if 1
  std::vector<int> a;
  std::vector<int> b;
  size = 80;
  for(int i = 0; i< size; i++){
    auto rand = random() % (3*size/2);
    a.push_back(rand);
    b.push_back(rand);
  }
  //quick_sort(a.begin(),a.end());
  quick_sort_hybrid(a.begin(),a.end(),a.end() - a.begin());
  //quick_sort_hybrid_analysis(a.begin(),a.end(),a.end() - a.begin());
  for(auto x: a) std::cout<<x<<" ";
  std::cout<<"\n";
  std::sort(b.begin(),b.end());
  std::cout<<"Our sort works? : "<<(a==b)<<"\n";
#endif
  //benchmarking
#if 0
  size =  100000000;
  std::vector<int> c(size);
  std::vector<int> d;

  for(int i = 0; i < size; i++){
      auto rand = random() % (3*size/2);
      c.at(i) = rand;
      //d.push_back(rand);
  }
  //quick_sort(c.begin(),c.end());
  stack_depth st;
  st.counter = 0;
  int max_depth = (int) 2.0*log(c.end() - c.begin())/log(2.0);
  //insertion_sort(c.begin(),c.end());
  quick_sort_hybrid_analysis(c.begin(),c.end());
  //std::sort(c.begin(),c.end());
  //for(auto x: c) std::cout<<x<<" ";
  std::cout<<"\n";


#endif

  return 0;
}
