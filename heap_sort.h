#ifndef H_HEAP_SORT_H
#define H_HEAP_SORT_H

class MaxHeap{
public:
  inline std::vector<int>::iterator parent(std::vector<int>::iterator i) { return begin + (i - begin - 1)/2; };
  inline std::vector<int>::iterator left_index(std::vector<int>::iterator i) { return begin + 2*(i - begin) + 1; };
  inline std::vector<int>::iterator right_index(std::vector<int>::iterator i) { return begin + 2*(i - begin) + 2; };
  inline void decr_heap() { heap_last--; } ;

  void heapify(std::vector<int>::iterator i)
  {
    auto l = left_index(i);
    auto r = right_index(i);
    std::vector<int>::iterator largest;
    
    if(l < heap_last && *l > *i){
      largest = l;
    }
    else largest = i;
    if(r < heap_last && *r > *largest){
      largest = r;
    }
    if (*largest != *i){
      auto tmp = *i;
      *i = *largest;
      *largest = tmp;
      heapify(largest);
    }
  }
  static MaxHeap build_heap(
                            std::vector<int>::iterator begin,
                            std::vector<int>::iterator end)
  {
    MaxHeap heap(begin, end);
    for(auto i = begin + (end - begin)/2 - 1; i >= begin; i--){
      heap.heapify(i);
    }
    return heap; // 1 5 2 10 4
  }
  
  static void heap_sort(
                        std::vector<int>::iterator begin,
                        std::vector<int>::iterator end)
  {
    MaxHeap heap = build_heap(begin,end);

    for(auto i = end - 1; i >= begin + 1; i--){      
      auto tmp = *begin;
      *begin = *i;
      *i = tmp;
      heap.decr_heap();
      heap.heapify(begin);
    }
  }

private:
  MaxHeap(std::vector<int>::iterator begin, std::vector<int>::iterator end) 
  { 
    this->begin = begin;
    this->end = end;
    this->heap_last = end;
  };
  void operator=(MaxHeap&);
  MaxHeap(const MaxHeap&);
  ~MaxHeap() {};
  std::vector<int>::iterator begin;
  std::vector<int>::iterator end;
  std::vector<int>::iterator heap_last;
};


#endif
