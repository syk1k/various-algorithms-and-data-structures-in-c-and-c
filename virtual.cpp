#include <iostream>

class A{
public:
    virtual void g(){
	std::cout << "Called from A\n";
    }
};
class B: public A{
public:
    virtual void g(){
	std::cout << "Called from B\n";
    }
};

class C{
public:
    void g(){
	std::cout << "Called from C\n";
    }
};
class D: public C{
public:
    void g(){
	std::cout << "Called from D\n";
    }
};


int main(){
    A* der1 = new B();
    der1->g();
    C* der2 = new D();
    der2->g();
    return 0;
}
