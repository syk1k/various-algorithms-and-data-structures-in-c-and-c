#include <iostream>

class A{
public:
    virtual void g() = 0;

};
class B: public A{
public:
    virtual void g(){
	std::cout << "I am Happy :)\n";
    }
};

int main(){
    A* der = new B();
    der->g();
    return 0;
}
