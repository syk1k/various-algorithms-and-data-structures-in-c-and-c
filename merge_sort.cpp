#include "stdafx.h"

//using iterators so-so
void merge(std::vector<int>& A, int p, int middle, int q)
{
  int n1 = middle - p + 1;
  int n2 = q - middle;

  // auxiliary arrays

  std::vector<int> L(n1); // left
  std::vector<int>::const_iterator l_begin(&A.at(p));
  std::vector<int>::const_iterator l_end(&A.at(middle));
  std::copy(l_begin,++l_end,L.begin());
  
  std::vector<int> R(n2); // right
  std::vector<int>::iterator r_begin(&A.at(middle +1));
  std::vector<int>::iterator r_end(&A.at(q));
  std::copy(r_begin,++r_end,R.begin());

  // main algorithm

  int i,j = 0;

  for(int k = p; k <= q; k++){
    if (i == n1){
      std::copy(std::vector<int>::iterator(&R.at(j)),R.end(),&A.at(k));
      break;
    }
    else if (j == n2){
      std::copy(std::vector<int>::iterator(&L.at(i)),L.end(),&A.at(k));
      break;
    }
    else if (L.at(i) <= R.at(j)){
      A.at(k) = L.at(i);
      i++;
    }
    else{
      A.at(k) = R.at(j);
      j++;
    }
  }
}

void merge_sort(std::vector<int>& A,int p, int q)
{
  int middle = floor((p+q)/2);
  if (p < q){
    merge_sort(A,p,middle);
    merge_sort(A,middle+1,q);
    merge(A,p,middle,q);
  }
}

// using iterators fully

void merge(
           std::vector<int>& A, 
           std::vector<int>::iterator p, 
           std::vector<int>::iterator middle, 
           std::vector<int>::iterator  q)
{
  int n1 = std::distance(p,middle);
  int n2 = std::distance(middle,q);
  // auxiliary arrays

  std::vector<int> L(n1); // left
  std::copy(p,middle,L.begin());
  
  std::vector<int> R(n2); // right
  std::copy(middle,q,R.begin()); /* middle is already incremented by 1 */

  // main algorithm

  std::vector<int>::iterator i(L.begin());
  std::vector<int>::iterator j(R.begin());

  for(std::vector<int>::iterator k = p; k < q; k++){
    if (i == L.end()){
      std::copy(j,R.end(),k);
      break;
    }
    else if (j == R.end()){
      std::copy(i,L.end(),k);
      break;
    }
    else if (*i <= *j){
      *k = *i;
      i++;
    }
    else{
      *k = *j;
      j++;
    }
  }
}

void merge_sort(std::vector<int>& A, std::vector<int>::iterator p, std::vector<int>::iterator q)
{
  int n = std::distance(p,q);
  std::vector<int>::iterator middle = p + n/2;
  
  if (q != p + 1){
    merge_sort(A,p,middle);
    merge_sort(A,middle,q);
    merge(A,p,middle,q);
  }
}

// algorithm with indices and arrays only

void merge(int A[], int p, int middle, int q) // arrays are always passed by reference
{
  //vars

  int n1 = middle - p + 1;
  int n2 = q - middle;
  
  // auxiliary arrays

  int *L = new int [n1];
  int *R = new int [n2];
  
  for (int _i = p; _i <= middle; _i++){
    L[_i-p] = A[_i];
  }

  for (int _j = middle + 1; _j <= q; _j++){
    R[_j-middle -1] = A[_j];
  }

  // main algorithm

  int i,j = 0;

  for(int k = p; k <= q; k++){
    if (i == n1){
      for(int _j = j; _j < n2; _j++) A[k++] = R[_j];
      break;
    }
    else if (j == n2){
      for(int _i = i; _i < n1; _i++) A[k++] = L[_i];
      break;
    }
    else if(L[i] <= R[j]){
      A[k] = L[i];
      i++;
    }
    else{
      A[k] = R[j];
      j++;
    }
  } 
  delete[] L;
  delete[] R; 
}

void merge_sort(int A[], int p, int q)
{
  //vars
  int middle = (p + q) / 2; 
  if (p < q){
    merge_sort(A,p,middle);
    merge_sort(A,middle+1,q);
    merge(A,p,middle,q);
  }
}

// version written on collabedit
// method that merges 2 sorted arrays i.e. std::vectors in c++
// do we want it templated?

std::vector<int> merge(
    std::vector<int>::iterator p, 
    std::vector<int>::iterator mid,
    std::vector<int>::iterator q)
{
    std::vector<int> B(q - p); // q - p is the size of the array p and q point to
    // use std convention - mid is an iterator to the element
    // 1 to the right of the middle element
    
    auto i = p;
    auto j = mid;
    
    for(auto k = B.begin(); k < B.end(); ++k){
        if(i == mid){
            std::copy(j,q,k);
            break;
        }
        else if (j == q){
            std::copy(i,mid,k);
            break;
        }
        else if(*i <= *j){
            *k = *i;
            i++;
        }
        else{
            *k = *j;
            j++;
        }
    }
    return B;
}

int main()
{
#if 0
  std::vector<int> A;
  std::vector<int> B(10000000);
  std::vector<int> C;
  int D[] = {17,4,6,2,21,1,9,0,10,30};
  int *E = new int[10000000];
  for(int i=0; i < 10000000; i++){
    int rand = random();    
    A.push_back(rand);
    E[i] = rand;
  }
  
  C.push_back(17); C.push_back(4); C.push_back(6); C.push_back(2);  C.push_back(21);
  C.push_back(1);  C.push_back(9); C.push_back(0); C.push_back(10); C.push_back(30);

  // std::cout<<"dist " <<std::distance(C.begin(),C.end())<< "\n";
  // int dist = std::distance(C.begin(),C.end());
  // std::vector<int>::iterator i1 = C.begin();
  // std::cout<< "first : " << *i1 << "\n";
  // std::cout<< "second : " << *(i1+1) << "\n";
  // std::cout<< "middle : " << *(i1 + dist/2) << "\n";
  // std::cout<< "dist begining to middle : " << std::distance(C.begin(), (i1 + dist/2)) << "\n";
  // std::cout<< "dist middle to end : " << std::distance((i1 + dist/2), C.end()) << "\n";
  //return 0;
  //merge_sort(C,0,C.size()-1);
  merge_sort(C,C.begin(),C.end());
  //merge_sort(A,0,A.size()-1);
  merge_sort(A,A.begin(),A.end());
  //merge_sort(E,0,10000000-1);

  std::copy(A.begin(),A.end(),B.begin());
  std::sort(B.begin(),B.end());
  std::cout << "iterators C \n";
  for(auto x: C) std::cout << x << " ";
  std::cout<<"\n";

  merge_sort(D,0,9);
  std::cout << "arrays D \n";
  for(auto x: D) std::cout << x << " ";
  std::cout<<"\n";


  std::cout<<"Is our merge sort the same as std::sort ? (Should be 1) "<< (A == B) << std::endl;
  A.push_back(10);
  std::cout<<"Is our merge sort different if we add another element? (Should be 0) " << (A == B) << std::endl;
#endif
#if 1
  std::vector<int> A;
  for(int i=0; i < 100000000; i++){
    A.push_back(random());
  }
  merge_sort(A,A.begin(),A.end());
#endif
  
  return 0;
}
