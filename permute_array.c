#include <stdio.h>
#include "perm_funs.h"
#include "math.h"

int
my_random (int, int);

void
swap (int*, int*);

void
permute (int a[], int len)
{
  for (int i = len -1; i > -1; --i)
    {
      int ran = my_random (0, i);
      swap (&a[i], &a[ran]);
    }
}
  
void
swap (int* a, int* b)
{
  int tmp;
  tmp = *a;
  *a = *b;
  *b = tmp;
}

int
my_random (int low, int high)
{
  double entr = my_rand();
  int n_buckets = high - low + 1;
  int i;
  double d = 1.0/n_buckets;
  for (i = 0; i < n_buckets; ++i)
    {
      if (entr > i * d && entr < (i + 1)*d)
	break;
    }
  
  return i + low;
  // or return low + floor(entr * n_buckets)
}

int
main ()
{
  int a[] = {1,2,3,4,5,6,7};
  permute (a, 7);
  for (int i = 0; i < 7; ++i)
    {
      printf("%d, ", a[i]);
    }
  printf("\n");
  
  return 0;
}
