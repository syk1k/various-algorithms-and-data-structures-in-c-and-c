#include <iostream> // ?
#include <vector>
#include <iterator>
#include <algorithm>

// method that merges 2 sorted arrays i.e. std::vectors in c++
// do we want it templated?

std::vector<int> merge(
    std::vector<int>::iterator p, 
    std::vector<int>::iterator mid,
    std::vector<int>::iterator q)
{
    std::vector<int> B(q - p); // q - p is the size of the array p and q point to
    // use std convention - mid is an iterator to the element
    // 1 to the right of the middle element
    
    auto i = p;
    auto j = mid;
    
    for(auto k = B.begin(); k < B.end(); k++){
        if(i == mid){
          std::copy(j,q,k);
          break;
        }
        else if (j == q){
          std::copy(i,mid,k);
          break;
        }
        else if(*i <= *j){
            *k = *i;
            i++;
        }
        else{
            *k = *j;
            j++;
        }
    }
    return B;
}

int main()
{
  std::vector<int> A;
  A.push_back(1);
  A.push_back(3);
  A.push_back(2);
  auto p = A.begin();
  auto q = A.end();
  auto mid = A.begin()+2;
  std::cout<<"mid is "<<*mid<<"\n";
  std::vector<int> B = merge(p,mid,q);
  
  for(auto x: merge(p,mid,q)) std::cout<<x<<" ";
  std::cout<<"\n";
  return 0;
}
