Sorting Algorithms and other in C++11/C
=====================================

This repository contains C++ and C implementations of popular algorithms. The stable code resides in the `master` branch while more code snippets can be found on `dev`.  

On Master:

Contains full C++ (using the C++11 standard) implementations of:

	Insertion sort - insertion_sort.cpp, insertion_sort.h
	Quicksort - quick_sort.cpp
	Mergesort - merge_sort.cpp, merge_alt.cpp
	Heapsort - heap_sort.cpp, heap_sort.h
	Counting sort - counting_sort.cpp, counting_sort.h
	Hybrid quicksort (switches to insertion sort on small arrays, switches to Heapsort if recursion too deep) - quicksort.cpp

also has a 
	
        C linked-list implementation (linked_list.c) with reverse operations(recursive and iterative), and whole bunch of other functions.
        depth-first-search graph traversal implementation - dfs.cpp
        palindrome checker for integers - is_palindrome.cpp
        dynamic programming Fibonacci number calculator - fibonacci_dp.cpp
        utility classes for counting sort - sorter.h

and a few other files.

To build, open a terminal, type

    make

, and tab to pick what to compile. You need gcc/g++ that supports C++11, but clang should compile them as well.

Planning to add unit tests (gtests) to lock the behaviour - this will be done in case I use these algorithms in another project.