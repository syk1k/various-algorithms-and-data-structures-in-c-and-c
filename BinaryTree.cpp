#include "stdafx.h"
#include <cstddef>
#include <queue>
class BinaryTree{
public:
  BinaryTree(int key): key(key), left(NIL), right(NIL), depth(-1)  {}
  BinaryTree(int key, BinaryTree* left, BinaryTree* right):
    key(key), left(left), right(right), depth(-1) {}
  static BinaryTree BinaryTreeFactory(std::vector<int> v);
  void insertKey(int key);
  void inorderWalk();
  void preorderWalk();
  void postorderWalk();
  void prettyPrintVertical();
  void prettyPrintHorizontal();
  void deleteKey(int key);
  inline void setLeft(BinaryTree* left){
    this->left = left;
  }
  inline void setRight(BinaryTree* left){
    this->right = right;
  }
  inline int getDepth(){
    // based on postoder walk
    int leftDepth = 0;
    int rightDepth = 0;
    if (this->left == NIL)
      leftDepth = 0;
    else if (this->left->depth == -1)
      leftDepth = this->left->getDepth();
    else
      leftDepth = this->left->depth;

    if (this->right == NIL)
      rightDepth = 0;
    else if (this->right->depth == -1)
      rightDepth = this->right->getDepth();
    else
      rightDepth = this->right->depth;
    int cur_depth = std::max(leftDepth, rightDepth) + 1;
    this->depth = cur_depth;
    return cur_depth;
  }
  bool isBalanced();
  static BinaryTree* fromSortedArray(std::vector<int>& arr, int low, int high);
  static constexpr std::nullptr_t NIL = nullptr;
private:
  int key;
  // we need back pointers to parents to be able to update the height
  int depth;
  BinaryTree* left;
  BinaryTree* right;
};

void BinaryTree::insertKey(int key){
  if (this->key == key){
    return;
  }
  else if(this->key > key){
    if(this->left == NIL){
      BinaryTree* left = new BinaryTree(key);
      this->left = left;
    }
    else{
      this->left->insertKey(key);
    }
  }
  else{
    if(this->right == NIL){
      BinaryTree* right  = new BinaryTree(key);
      this->right = right;
    }
    else{   
      this->right->insertKey(key);
    }
  }
}

void BinaryTree::findKey(int key){
  if (this->key == key){
    return true;
  }
  else if(this->key > key){
    if(this->left == NIL){
      BinaryTree* left = new BinaryTree(key);
      this->left = left;
    }
    else{
      this->left->insertKey(key);
    }
  }
  else{
    if(this->right == NIL){
      BinaryTree* right  = new BinaryTree(key);
      this->right = right;
    }
    else{   
      this->right->insertKey(key);
    }
  }
  return false;
}


BinaryTree BinaryTree::BinaryTreeFactory(std::vector<int> v){
  if (v.size() == 0){
    std::cerr<<"Error - cannot construct binary tree from empty vector\n";
    std::exit(1);
  }
  int fst = v.at(0);
  BinaryTree bt(fst);
  for (auto key: v){
    bt.insertKey(key);
  }
  return bt;
}

BinaryTree* BinaryTree::fromSortedArray(std::vector<int>& arr, int low, int high)
{
  if (low > high)
    return BinaryTree::NIL;
  int middle = (high + low)/2;
  BinaryTree* bt = new BinaryTree (arr[middle]);
  if (arr[middle] == 7)
    {
      std::cout << low << " " << high << std::endl;
    }
  BinaryTree* left = fromSortedArray(arr, low, middle-1);
  BinaryTree* right = fromSortedArray(arr, middle + 1, high);
  bt->setLeft(left);
  bt->setRight(right);
  return bt;
}

void BinaryTree::inorderWalk(){
  if (this->left != NIL) this->left->inorderWalk();
  std::cout<<this->key<<" ";
  if (this->right != NIL) this->right->inorderWalk();
}

void BinaryTree::preorderWalk(){
  std::cout<<this->key<<" ";
  if (this->left != NIL) this->left->preorderWalk();
  if (this->right != NIL) this->right->preorderWalk();
}

static void pp_tab(int x){
  for (int i = 0; i < x; ++i) std::cout<<'\t';
}

static void pp_newline(){
  std::cout<<std::endl;
}

//TODO - fix
void BinaryTree::prettyPrintVertical(){
  //based on preorderWalk
  pp_tab(this->depth);
  std::cout<<this->key<<" ";
  pp_newline();
  if (this->left != NIL) this->left->prettyPrintVertical();
  pp_tab(1);
  if (this->right != NIL) this->right->prettyPrintVertical();

}

// TODO - fix
void BinaryTree::prettyPrintHorizontal(){
  // based on BFS
  int d = this->getDepth();
  std::queue<BinaryTree*> curLevel;
  std::queue<BinaryTree*> nextLevel;
  curLevel.push(this);
  pp_tab(d);
  while(!curLevel.empty()){
    BinaryTree* bt = curLevel.front();
    std::cout<< bt->key << "\t\t" ;
    curLevel.pop();
    if (bt->left != NIL) nextLevel.push(bt->left);
    if (bt->right != NIL) nextLevel.push(bt->right);
    if (curLevel.empty()) {
      std::swap(curLevel, nextLevel);
	std::cout << "\n";
	--d;
	pp_tab(d);
    }
  }
  std::cout << "\n";
}

void BinaryTree::postorderWalk(){
  if (this->left != NIL) this->left->postorderWalk();
  if (this->right != NIL) this->right->postorderWalk();
  std::cout<<this->key<<" ";
}

bool BinaryTree::isBalanced(){
  int leftDepth, rightDepth;
  bool isBalancedLeft, isBalancedRight;
  if (this->left == NIL){
    leftDepth = 0;
    isBalancedLeft = true;
  }
  else {
    leftDepth = this->left->getDepth();
    isBalancedLeft = this->left->isBalanced();
  }
  if (this->right == NIL){
    rightDepth = 0;
    isBalancedRight = true;
  }
  else {
    rightDepth = this->right->getDepth();
    isBalancedRight = this->right->isBalanced();
  }
  bool isBalanced = std::abs(leftDepth - rightDepth) <= 1;
  return isBalanced && isBalancedLeft && isBalancedRight;
}

int main(){
  std::vector<int> v {1,2,3,5,3,4,5,7,8,5,4,2,4,5,5,7};
  BinaryTree bt = BinaryTree::BinaryTreeFactory(v);
  bt.inorderWalk();
  std::cout<<std::endl;
  bt.preorderWalk();
  std::cout<<std::endl;
  bt.postorderWalk();
  std::cout<<std::endl;
  std::cout << bt.getDepth() << "\n";
  // pp_newline();
  // bt.prettyPrintVertical();
  // pp_newline();
  bt.prettyPrintHorizontal();
  std::cout << bt.isBalanced() << "\n";
  std::cout << v.size() << "\n";
  std::sort(v.begin(), v.end());
  BinaryTree* bt2 = BinaryTree::fromSortedArray (v, 0, v.size()-1);
  std::cout << bt2->isBalanced() << "\n";
  bt2->insertKey(7);
  bt2->prettyPrintHorizontal();
  std::cout << bt2->getDepth() << "\n";
  return 0;
}
