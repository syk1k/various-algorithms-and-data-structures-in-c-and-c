#include "stdafx.h"

std::vector<int> adjacentEdges(const std::vector<std::vector<int>>& G, int v);

// has to implemented via a queue, can't be done with recursion/a stack
void bfs(const std::vector<std::vector<int>>& G, int v, std::vector<int>& m,int visited)
{

}

bool alreadyVisited(const std::vector<int>& visited, int root);
void dfs_preorder(int root, const std::vector<std::vector<int>>& G, std::vector<int>& visited)
{
  visited.push_back(root);
  for (auto v: adjacentEdges(G, root))
    if (!alreadyVisited(visited, v))
      dfs_preorder(v,G,visited);
}

// TODO - fix infinite recursion
void dfs_postorder(int root, const std::vector<std::vector<int>>& G, std::vector<int>& visited)
{
  for (auto v: adjacentEdges(G, root))
    if (!alreadyVisited(visited, v)){
      visited.push_back(v);
      dfs_postorder(v,G,visited);
    }
}

std::vector<int> adjacentEdges(const std::vector<std::vector<int>>& G, int v)
{
  std::vector<int> r;
  for(int i = 0; i < G.size(); ++i){
    if(G.at(v).at(i) == 1 && i != v) r.push_back(i);
  }
  return r;
}

bool alreadyVisited(const std::vector<int>& visited, int root)
{
  for (auto&v: visited)
    if (v == root)
      return true;
  return false;
}

void printVec(std::vector<int>& v)
{
  std::cout << "Printing order of visiting:\n";
  for (auto& e: v)
    std::cout << e << " ";
  std::cout << "\n";
}

bool routeExists(int p, int v, const std::vector<std::vector<int>>& G)
{
  std::vector<int> visited;
  dfs_preorder(p, G, visited);
  return alreadyVisited(visited, v);
}

int main()
{
  std::vector<std::vector<int>> G{{0,0,1,1},{0,1,0,0},{1,1,0,0},{1,0,0,0}};
  std::vector<int> m;
  dfs_preorder(0,G,m);
  printVec(m);
  std::vector<int> m1;
  dfs_postorder(0,G,m1);
  printVec(m1);
  std::cout << routeExists(1,0, G) << "\n";
  std::cout << routeExists(0,1, G) << "\n";
  return 0;
}

/*
  

 */








