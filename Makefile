stdafx: stdafx.cpp stdafx.h
	g++ -c -std=c++11 -o stdafx.o stdafx.cpp
insertion_sort: insertion_sort.cpp 
	g++ -c -std=c++11 -O3 -o bin/insertion_sort.o insertion_sort.cpp -I./ -lpthread
	g++ -o bin/insertion_sort bin/insertion_sort.o
merge_sort: merge_sort.cpp  stdafx.cpp
	g++ -std=c++0x -O3 -o merge_sort merge_sort.cpp stdafx.cpp -lpthread
heap_sort: heap_sort.cpp  stdafx.cpp heap_sort.h
	g++ -std=c++0x -O3 -o heap_sort heap_sort.cpp stdafx.cpp -lpthread
quick_sort: quick_sort.cpp stdafx.cpp insertion_sort.cpp insertion_sort.h heap_sort.h partition.h partition.cpp
	clang++ -std=c++11 -O3 -o quick_sort quick_sort.cpp stdafx.cpp insertion_sort.cpp heap_sort.cpp partition.cpp
counting_sort: counting_sort.cpp stdafx.cpp  counting_sort.h
	g++ -std=c++0x -O3 -o counting_sort counting_sort.cpp stdafx.cpp
fibonacci_dp: fibonacci_dp.cpp stdafx.cpp 
	g++ -std=c++11 -o fib.exe -I./ fibonacci_dp.cpp
graphs_tests: graphs_tests.cpp stdafx.cpp Graph.h
	g++ -std=c++11 -O3 -o graphs_tests graphs_tests.cpp stdafx.cpp		
is_palindrome: is_palindrome.cpp stdafx.cpp 
	g++ -std=c++0x -O3 -o is_palindrome is_palindrome.cpp stdafx.cpp			
linked_list.c: linked_list.c
	gcc -c -std=c99 -o bin/linked_list.o linked_list.c -D__NUM=20 -D__DEBUG -D_XOPEN_SOURCE=600
	gcc -o bin/linked_list bin/linked_list.o
select: select.cpp partition.h partition.cpp
	clang++ -std=c++11 -O1 -o select select.cpp partition.cpp -I./
select-debug: select.cpp partition.h partition.cpp
	clang++ -std=c++11 -O0 -g -o select select.cpp partition.cpp -I./
clean:
	rm bin/*
binary_tree: BinaryTree.cpp stdafx.cpp stdafx.h
	g++ -std=c++11 -ggdb3 -O0 -o binary_tree BinaryTree.cpp stdafx.cpp
dfs: dfs.cpp
	g++ -O0 -ggdb3 -std=c++11 -o dfs dfs.cpp
